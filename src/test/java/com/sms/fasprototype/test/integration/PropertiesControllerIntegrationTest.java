package com.sms.fasprototype.test.integration;

import com.sms.fasprototype.config.Application;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * PropertiesControllerIntegrationTest
 * @author cmoten
 * 
 * Provides integration testing of the PropertiesController REST endpoints
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
public class PropertiesControllerIntegrationTest {
    
    /**
     * MockMvc object
     */
    private MockMvc mockMvc;
    
    /**
     * WebApplicationContext object
     */
    @Autowired
    private WebApplicationContext webAppCtx;
    
    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webAppCtx).build();
    }
    
    /* ***************************** Tests ********************************** */
    
    /**
     * Tests /appVersion REST endpoint
     * @throws Exception 
     */
    @Test
    public void testGetAppVersion() throws Exception{
        //Perform a GET request to /appVersion REST endpoint in PropertiesController
        MvcResult result = mockMvc.perform(get("/appVersion"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.data", Matchers.notNullValue()))
                .andExpect(jsonPath("$.status", Matchers.equalTo(true)))
                .andExpect(jsonPath("$.message", Matchers.equalTo("Request completed successfully!")))
                .andReturn();
        
        System.out.println("App Version: " + result.getResponse().getContentAsString());
    }
}
