/* 
 * FileSearch
 * 
 * Defines file search widget components (TextBox and Button). These components comprise the main search tool
 * for querying for file (SSN) numbers for FAS operations.
 */
define([
    'dijit/form/TextBox',
    'dijit/form/Button',
    'dojo/domReady!'
], function(TextBox, Button){
    var searchTextBox = new TextBox({
        id: 'fileSearchBox',
        class: 'fileSearchBox',
        placeHolder: 'Search for file #...'
    });
    
    var searchButton = new Button({
        id: 'fileSearchBtn',
        class: 'fileSearchBtn',
        iconClass: 'dijitIconSearch',
        showLabel: true,
        label: 'Search',
        onClick: function() {
            var fileNum = searchTextBox.get('value');
            console.log('This will search for file #: ' + fileNum);
        }
    });
    
    return {
        getFileSearchBox: function(){
            return searchTextBox;
        },
        getFileSearchButton: function(){
            return searchButton;
        }
    };
});


