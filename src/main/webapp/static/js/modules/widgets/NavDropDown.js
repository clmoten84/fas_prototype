/* 
 * NavDropDown
 * 
 * Defines a drop down widget used for navigating between
 * different FAS portlets.
 */
define([
   'dijit/form/DropDownButton',
   'dijit/DropDownMenu',
   'dijit/MenuItem',
   'dijit/PopupMenuItem',
   'portlets/FASReports',
   'portlets/FASAwards',
   'portlets/FASNotes',
   'portlets/FASSam'
], function(DropDownButton, DropDownMenu, MenuItem, 
                PopupMenuItem, FASReports, FASAwards,
                FASNotes, FASSam){
    /* Top level drop down menu */
    var topMenu = new DropDownMenu({class: 'navMenu'});
    
    /* CWINRS/SAM menu item */
    var cwinrsMenuItem = new MenuItem({
        label: 'FAS CWINRS/SAM',
        iconClass: 'dijitIconPackage',
        onClick: function(){
            navButton.set('label', 'FAS CWINRS/SAM');
            navButton.set('iconClass', this.iconClass);
            
            //Display FAS CWINRS/SAM portlet
            FASSam.buildFASSam();
        }
    });
    topMenu.addChild(cwinrsMenuItem);
    
    /* FAS Notes menu item */
    var notesMenuItem = new MenuItem({
        label: 'FAS Notes',
        iconClass: 'dijitIconTask',
        onClick: function(){
            navButton.set('label', 'FAS Notes');
            navButton.set('iconClass', this.iconClass);
            
            //Display FAS Notes portlet
            FASNotes.buildFASNotes();
        }
    });
    topMenu.addChild(notesMenuItem);
    
    /* FAS Awards menu item */
    var awardsMenuItem = new MenuItem({
        label: 'FAS Awards',
        iconClass: 'dijitIconPackage',
        onClick: function(){
            navButton.set('label', 'FAS Awards');
            navButton.set('iconClass', this.iconClass);
            
            //Display FAS Awards portlet
            FASAwards.buildFASAwards();
        }
    });
    topMenu.addChild(awardsMenuItem);
    
    /* FAS Reports menu */
    var reportsMenuItem = new MenuItem({
        label: 'FAS Reports',
        iconClass: 'dijitLeaf',
        onClick: function(){
            navButton.set('label', 'FAS Reports');
            navButton.set('iconClass', this.iconClass);
            
            //Display FAS Reports portlet
            FASReports.buildFASReports();
        }
    });
    topMenu.addChild(reportsMenuItem);
    
//    /* FAS Reports sub menu */
//    var reportsSubMenu = new DropDownMenu({class: 'navMenu'});
//    
//    /* C&P Services Reports menu item */
//    var cpServicesReportsMenuItem = new MenuItem({
//        label: 'C&P Services Reports',
//        iconClass: 'dijitLeaf',
//        onClick: function(){
//            navButton.set('label', 'C&P Services Reports');
//            navButton.set('iconClass', this.iconClass);
//        }
//    });
//    reportsSubMenu.addChild(cpServicesReportsMenuItem);
//    
//    var eduReportsMenuItem = new MenuItem({
//        label: 'CH31 EDU Reports',
//        iconClass: 'dijitLeaf',
//        onClick: function(){
//            navButton.set('label', 'CH31 EDU Reports');
//            navButton.set('iconClass', this.iconClass);
//        }
//    });
//    reportsSubMenu.addChild(eduReportsMenuItem);
//    
//    /* Add sub-menu to top-level menu */
//    topMenu.addChild(new PopupMenuItem({
//        label: 'FAS Reports',
//        iconClass: 'dijitLeaf',
//        popup: reportsSubMenu
//    }));
    
    var navButton = new DropDownButton({
        label: 'Select a FAS Application...',
        name: 'fasNavBtn',
        id: 'fasNavBtn',
        dropDown: topMenu
    });
    
    return {
        getNavButton: function(){
            return navButton;
        }
    };
});


