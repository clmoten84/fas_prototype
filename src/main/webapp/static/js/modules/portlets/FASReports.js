/* 
 * FASReports.js
 * 
 * Defines components for FAS Reports portlet. This portlet
 * provides FAS reports functionality (C&P Services and CH31 EDU). 
 * This module defines the necessary UI components and places them 
 * inside the FAS portlet container.
 */
define([
    'dojo/_base/array',
    'dijit/dijit',
    'dijit/layout/TabContainer',
    'dijit/layout/BorderContainer'
], function(array, dijit, TabContainer, BorderContainer){
    
    var buildPortlet = function(){
        /* Tab Container for C&P Services and CH31 EDU Reports portlets */
        var reportsTabContainer = new TabContainer({
            id: 'reportsTabContainer',
            tabStrip: true,
            persist: true,
            tabPosition: 'top',
            region: 'center'
        });

        /* Portlet for C&P Services */
        var cpsReportsPortlet = new BorderContainer({
            title: 'C&P Services Reports',
            id: 'cpsReportsPortlet',
            class: 'portlet',
            design: 'headline',
            gutters: true
        });
        reportsTabContainer.addChild(cpsReportsPortlet);

        var eduReportsPortlet = new BorderContainer({
            title: 'CH 31 EDU Reports',
            id: 'eduReportsContainer',
            class: 'portlet',
            design: 'headline',
            gutters: true
        });
        reportsTabContainer.addChild(eduReportsPortlet);
        
        return reportsTabContainer;
    };
    
    return {
        buildFASReports: function(){
            var portletContainer = dijit.byId('fasPortletContainer');
            portletContainer.set('class', 'portletContainerActive');
            
            if(portletContainer.hasChildren()){
                portletContainer.removeChild(portletContainer.getChildren()[0]);
            }
            
            //Add Reports tab container portlet to portlet container
            if(dijit.byId('reportsTabContainer')){
                portletContainer.addChild(dijit.byId('reportsTabContainer'));
            }
            else {
                portletContainer.addChild(buildPortlet());
            }
        }
    };
});


