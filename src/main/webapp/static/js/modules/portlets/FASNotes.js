/* 
 * FASNotes.js
 * 
 * Defines components for FAS Notes portlet. This portlet
 * provides FAS Notes functionality. This module defines the 
 * neccessary UI components and places them inside the FAS
 * portlet container for display.
 */

define([
    'dijit/dijit',
    'dijit/layout/BorderContainer',
    'dijit/layout/ContentPane'
], function(dijit, BorderContainer, ContentPane){
    var buildPortlet = function(){
        /* FAS Notes portlet */
        var notesPortlet = new BorderContainer({
            id: 'fasNotesPortlet',
            class: 'portlet',
            design: 'headline',
            gutters: true,
            region: 'center'
        });
        
        /* FAS Notes Content Pane */
        var notesCenterContentPane = new ContentPane({
            id: 'fasNotesCenterContentPane',
            region: 'center',
            content: '<p><b>FAS Notes content will go here...</b></p>'
        });
        notesPortlet.addChild(notesCenterContentPane);
        
        return notesPortlet;
    };
    
    return {
        buildFASNotes: function(){
            var portletContainer = dijit.byId('fasPortletContainer');
            portletContainer.set('class', 'portletContainerActive');
            
            if(portletContainer.hasChildren()){
                portletContainer.removeChild(portletContainer.getChildren()[0]);
            }
            
            //Add FAS Notes portlet to portlet container
            if(dijit.byId('fasNotesPortlet')){
                portletContainer.addChild(dijit.byId('fasNotesPortlet'));
            }
            else {
                portletContainer.addChild(buildPortlet());
            }
        }
    };
});


