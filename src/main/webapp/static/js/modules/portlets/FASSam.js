/* 
 * FASSam.js
 * 
 * Defines components for CWINRS/SAM portlet. This portlet provides
 * CWINRS/SAM functionality. This module defines the neccessary UI
 * components and places them inside the FAS portlet container for
 * display.
 */
define([
    'dijit/dijit',
    'dijit/layout/BorderContainer',
    'dijit/layout/ContentPane'
], function(dijit, BorderContainer, ContentPane){
    var buildPortlet = function(){
        /* FAS CWINRS/SAM portlet */
        var samPortlet = new BorderContainer({
            id: 'fasSamPortlet',
            class: 'portlet',
            design: 'headline',
            gutters: true,
            region: 'center'
        });
        
        /* FAS CWINRS/SAM Content Pane */
        var samCenterContentPane = new ContentPane({
            id: 'fasCenterSamContentPane',
            region: 'center',
            content: '<p><b>FAS CWINRS/SAM content will go here...</b></p>'
        });
        samPortlet.addChild(samCenterContentPane);
        
        return samPortlet;
    };
    
    return {
        buildFASSam: function(){
            var portletContainer = dijit.byId('fasPortletContainer');
            portletContainer.set('class', 'portletContainerActive');
            
            if(portletContainer.hasChildren()){
                portletContainer.removeChild(portletContainer.getChildren()[0]);
            }
            
            //Add FAS CWINRS/SAM portlet to portlet container
            if(dijit.byId('fasSamPortlet')){
                portletContainer.addChild(dijit.byId('fasSamPortlet'));
            }
            else {
                portletContainer.addChild(buildPortlet());
            }
        }
    };
});

