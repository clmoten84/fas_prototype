/* 
 * FASAwards.js
 * 
 * Defines components for FAS Awards portlet. This portlet
 * provides FAS Awards functionality. This modules defines
 * the necessary UI components and places them inside the FAS
 * portlet container for display.
 */
define([
    'dijit/dijit',
    'dijit/layout/BorderContainer',
    'dijit/layout/ContentPane'
], function(dijit, BorderContainer, ContentPane){
    
    var buildPortlet = function(){
        /* FAS Awards Portlet */
        var awardsPortlet = new BorderContainer({
            id: 'fasAwardsPortlet',
            class: 'portlet',
            design: 'headline',
            gutters: true,
            region: 'center'
        });
        
        /* Awards Content Pane */
        var awardsCenterPane = new ContentPane({
            region: 'center',
            id: 'fasAwardsCenterContent',
            content: '<p><b>FAS Awards content will go here...</b></p>'
        });
        awardsPortlet.addChild(awardsCenterPane);
        
        return awardsPortlet;
    };
    
    return {
        buildFASAwards: function(){
            var fasPortletContainer = dijit.byId('fasPortletContainer');
            fasPortletContainer.set('class', 'portletContainerActive');
            
            if(fasPortletContainer.hasChildren()){
                fasPortletContainer.removeChild(fasPortletContainer.getChildren()[0]);
            }
            
            //Add FAS Award portlet to portlet container
            if(dijit.byId('fasAwardsPortlet')){
                fasPortletContainer.addChild(dijit.byId('fasAwardsPortlet'));
            }
            else {
                fasPortletContainer.addChild(buildPortlet());
            }
        }
    };
});


