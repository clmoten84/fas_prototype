/* 
 * AppProperties
 * 
 * This module defines functions for retrieving application
 * properties from the server-side.
 */

define([
    'dojo/request',
    'dijit/dijit'
], function(request, dijit){
    
    /*
     * getAppVersion
     * 
     * Makes a REST call to /appVersion endpoint to retrieve
     * app version data from server-side. Places this info in the
     * footer pane.
     */
    var getAppVersion = function(){
        request.get('/appVersion', {
            handleAs: 'json',
            headers: {
                "Content-Type": "application/json", 
                "Accept": "application/json"
            }
        }).then(function(data){
            var appProps = data.data;
            var footerPane = dijit.byId('footerPane');
            footerPane.set('content', '<div class="footerContent"><p><b>FAS Application Version: ' + appProps.applicationVersion + 
                    '</b><br><b>Build Date: ' + appProps.buildDate + '</b></p></div>');
        }, function(err){
            console.log(err);
            var footerPane = dijit.byId('footerPane');
            footerPane.set('content', '<div class="footerContent"><p><b>FAS Application Version: ERROR' + 
                    '</b><br><b>Build Date: ERROR</b></p></div>');
        });
    };
    
    //Return all functions as members of return object
    return {
        getAppVersion: getAppVersion
    };
});


