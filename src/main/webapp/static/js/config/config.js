/* 
 * config.js
 * 
 * Dojo configuration file. Configures dojo environment including modules
 * and where to find them.
 */

//Global dojo config object
var dojoConfig = {
    async: true,
    baseUrl: '.',
    packages: [
        {name: 'dojo', location: 'static/vendor/dojo/dojo'},
        {name: 'dijit', location: 'static/vendor/dojo/dijit'},
        {name: 'utilities', location: 'static/js/modules/utilities'},
        {name: 'widgets', location: 'static/js/modules/widgets'},
        {name: 'portlets', location: 'static/js/modules/portlets'}
    ]
};
