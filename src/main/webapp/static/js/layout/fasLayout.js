/* 
 * FAS application layout
 * 
 * Builds the page layout for FAS. FAS is built as a single page application
 * and this file defines the layout for the single page. This file gets loaded
 * in index.html.
 */

require([
    'dijit/layout/BorderContainer',
    'dijit/layout/ContentPane',
    'dijit/layout/TabContainer',
    'utilities/AppProperties',
    'widgets/FileSearch',
    'widgets/NavDropDown'
], function(BorderContainer, ContentPane, TabContainer, AppProperties, FileSearch, NavDropDown){
    
    /* *************************** MAIN PAGE CONTAINER ************************************* */
    var appLayout = new BorderContainer({
        design: 'headline',
        gutters: false
    }, 'appLayout');
    
    /* *************************** PAGE HEADER PANEL AND CONTENTS ******************************* */
    var headerContainer = new BorderContainer({
        id: 'headerContainer',
        region: 'top',
        design: 'headline',
        gutters: false
    });
    appLayout.addChild(headerContainer);
    
    /* VA Seal and Divider */
    var leftHeaderSection = new ContentPane({
        id: 'leftHeaderSection',
        region: 'left',
        content: '<img src="/static/images/VA_Seal_128.png" alt="VA Seal" class="vaSeal"/>\n\
                    <span><div class="headerDivider"></div></span>'
    });
    headerContainer.addChild(leftHeaderSection);
    
    /* Center Header Section */
    var centerHeaderSection = new ContentPane({
        id: 'centerHeaderSection',
        region: 'center',
        content: '<h2 class="title">FAS | VA Financial Accounting Services</h2>'
    });
    headerContainer.addChild(centerHeaderSection);
    
    /* File Search Section of Center Section */
    var fileSearchSection = new ContentPane({
        id: 'fileSearchSection'
    });
    centerHeaderSection.addChild(fileSearchSection);
    
    fileSearchSection.addChild(FileSearch.getFileSearchBox());
    fileSearchSection.addChild(FileSearch.getFileSearchButton());
    
    /* Right Header Section */
    var rightHeaderSection = new ContentPane({
        id: 'rightHeaderSection',
        region: 'right'
    });
    headerContainer.addChild(rightHeaderSection);
    
    /* ******************************** PAGE FOOTER PANEL AND CONTENTS ********************************** */
    var footerPane = new ContentPane({
        id: 'footerPane',
        region: 'bottom',
        design: 'headline'
    });
    appLayout.addChild(footerPane);
    
    //Get application version and build date and populate footerPane with it
    AppProperties.getAppVersion();
    
    /* ******************************** CONTENT CONTAINER AND CONTENTS ********************************** */
    var contentContainer = new BorderContainer({
        id: 'contentContainer',
        design: 'headline',
        region: 'center',
        gutters:true
    });
    appLayout.addChild(contentContainer);
    
    //Nav Container and Content Pane
    var navContainer = new BorderContainer({
        id: 'navContainer',
        region: 'top',
        design: 'headline'
    });
    contentContainer.addChild(navContainer);
    
    var navPane = new ContentPane({
        id: 'navPane',
        region: 'left'
    });
    navPane.addChild(NavDropDown.getNavButton());
    navContainer.addChild(navPane);
    
    /* FAS Reports Content Pane */
    
    /* CWINRS/SAM Content Pane */
    
    /* FAS Awards Content Pane */
    
    /* FAS Notes Content Pane */
    
    /* FAS Portlet Container */
    var fasPortletContainer = new BorderContainer({
        id: 'fasPortletContainer',
        region: 'center',
        design: 'headline',
        gutters: true
    });
    contentContainer.addChild(fasPortletContainer);
    
    // Center Pane
//    var centerPane = new ContentPane({
//        id: 'centerContentPane',
//        region: 'center'
//    });
//    contentContainer.addChild(centerPane);
    
    /* ************************* START MAIN CONTAINER ****************************** */
    appLayout.startup();
});


