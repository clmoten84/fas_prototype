package com.sms.fasprototype.service;

import com.sms.fasprototype.controller.PropertiesController;
import com.sms.fasprototype.dto.AppPropertiesDTO;
import java.io.IOException;
import java.util.Properties;
import org.springframework.stereotype.Service;

/**
 * PropertiesService
 * @author cmoten
 * 
 * Service class handles business logic for retrieving application properties
 * from properties files that reside on the classpath.
 */
@Service
public class PropertiesService {
    
    /**
     * Retrieve the application version and build date properties from
     * the application properties file.
     * @return AppPropertiesDTO
     * @throws IOException 
     */
    public AppPropertiesDTO getAppVersion() throws IOException{
        Properties props = new Properties();
        String version, buildDate = "";
        
        props.load(PropertiesController.class.getClassLoader().getResourceAsStream("application.properties"));
        version = props.getProperty("application.version", "[DEFAULT_VERSION]");
        buildDate = props.getProperty("build.date", "[DEFAULT_BUILD_DATE]");
        
        return new AppPropertiesDTO(version, buildDate);
    }
}
