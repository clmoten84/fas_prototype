package com.sms.fasprototype.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

/**
 * ThymeLeafConfig
 * @author cmoten
 * 
 * Configures Thymeleaf templating. Specifies where Spring can resolve 
 * ThymeLeaf templates and the template engine to use. This class was defined
 * in order to override the default Thymeleaf template location. The default
 * location for templates was src/main/resources and now the location of templates
 * is /WEB-INF/templates.
 */
@Configuration
public class ThymeLeafConfig implements ApplicationContextAware{
    private ApplicationContext appCtx;
    
    @Override
    public void setApplicationContext(ApplicationContext appCtx){
        this.appCtx = appCtx;
    }
    
    @Bean
    public ViewResolver viewResolver(){
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngine());
        viewResolver.setCharacterEncoding("UTF-8");
        return viewResolver;
    }
    
    @Bean
    public SpringTemplateEngine templateEngine(){
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver());
        return templateEngine;
    }
    
    @Bean
    public ITemplateResolver templateResolver(){
        SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
        resolver.setApplicationContext(appCtx);
        resolver.setPrefix("/WEB-INF/templates/");
        resolver.setSuffix(".html");
        return resolver;
    }
}
