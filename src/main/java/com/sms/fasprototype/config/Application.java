package com.sms.fasprototype.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Application
 * @author cmoten
 * 
 * Spring Boot application driver. This class runs the 
 * Spring Boot application. Also annotated to enable Spring
 * auto configuration, and to enable component scanning in the
 * base application package.
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("com.sms.fasprototype")
public class Application {
    public static void main(String[] args){
        SpringApplication.run(Application.class, args);
    }
}
