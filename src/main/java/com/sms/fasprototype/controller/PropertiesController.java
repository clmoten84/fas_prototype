package com.sms.fasprototype.controller;

import com.sms.fasprototype.dto.AppPropertiesDTO;
import com.sms.fasprototype.dto.ResultsDTO;
import com.sms.fasprototype.service.PropertiesService;
import java.io.IOException;
import java.util.Properties;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * PropertiesController
 * @author cmoten
 * 
 * Controller handles retrieving displayable properties from 
 * application properties files. This controller DOES NOT handle
 * retrieving data source properties. Those are retrieved in the 
 * data source configuration class using the Spring environment 
 * and PropertySource.
 */
@RestController
public class PropertiesController {
    
    @Autowired
    private PropertiesService propsService;
    
    /**
     * Retrieves application version  and last build date from application.properties file
     * @return application version
     */
    @RequestMapping(value = "/appVersion", method = RequestMethod.GET)
    @ResponseBody
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    @Produces(MediaType.APPLICATION_JSON_VALUE)
    public ResultsDTO<AppPropertiesDTO> getAppVersion(){
        ResultsDTO<AppPropertiesDTO> results = new ResultsDTO();
        
        try {
            results.setData(propsService.getAppVersion());
            results.setStatus(true);
            results.setMessage("Request completed successfully!");
            results.setExceptionThrown(false);
            results.setExceptionMessage(null);
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            
            results.setExceptionThrown(true);
            results.setExceptionMessage(ex.getMessage());
        }
        return results;
    }
}
