package com.sms.fasprototype.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * HomeController
 * @author cmoten
 * 
 * Controller that loads the home page for the application.
 */
@Controller
public class HomeController {
    
    /**
     * Loads the home page template 
     * @return String representing the home page template
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(){
        return "index";
    }
}
