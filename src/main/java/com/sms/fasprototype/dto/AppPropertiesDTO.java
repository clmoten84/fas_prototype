package com.sms.fasprototype.dto;

/**
 * AppPropertiesDTO
 * @author cmoten
 * 
 * Encapsulates application and environment properties for transfer to 
 * client side.
 */
public class AppPropertiesDTO {
    private String applicationVersion;
    private String buildDate;
    
    public AppPropertiesDTO() {}
    
    public AppPropertiesDTO(String applicationVersion, String buildDate){
        this.applicationVersion = applicationVersion;
        this.buildDate = buildDate;
    }
    
    public String getApplicationVersion(){
        return applicationVersion;
    }
    
    public void setApplicationVersion(String applicationVersion){
        this.applicationVersion = applicationVersion;
    }
    
    public String getBuildDate(){
        return buildDate;
    }
    
    public void setBuildDate(String buildDate){
        this.buildDate = buildDate;
    }
}
