package com.sms.fasprototype.dto;

/**
 * ResultsDTO
 * @author cmoten
 * @param <E> represents data encapsulated by this DTO
 * 
 * DTO that encapsulates request results before sending back to client.
 */
public class ResultsDTO<E> {
    private E data;
    private boolean status;
    private boolean exceptionThrown;
    private String message;
    private String exceptionMessage;
    
    public ResultsDTO() {
        this.data = null;
        this.status = false;
        this.message = "Request failed to complete!";
        this.exceptionThrown = false;
    }
    
    public ResultsDTO(E data, boolean status, String message){
        this.data = data;
        this.status = status;
        this.message = message;
    }
    
    public E getData(){
        return data;
    }
    
    public void setData(E data){
        this.data = data;
    }
    
    public boolean getStatus(){
        return status;
    }
    
    public void setStatus(boolean status){
        this.status = status;
    }
    
    public boolean getExceptionThrown(){
        return exceptionThrown;
    }
    
    public void setExceptionThrown(boolean exceptionThrown){
        this.exceptionThrown = exceptionThrown;
    }
    
    public String getMessage(){
        return message;
    }
    
    public void setMessage(String message){
        this.message = message;
    }
    
    public String getExceptionMessage(){
        return exceptionMessage;
    }
    
    public void setExceptionMessage(String exceptionMessage){
        this.exceptionMessage = exceptionMessage;
    }
}
